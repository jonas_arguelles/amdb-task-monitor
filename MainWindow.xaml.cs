﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Diagnostics;

namespace AMDB_Task_Monitor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool prevPlayStatus = true;
        bool prevPauseStatus = false;

        int currentTask = 0;
        Database db = new Database();
        List<Tasks> taskList = new List<Tasks>();
        List<Tasks> dispTaskList;

        System.Windows.Forms.NotifyIcon tBarIcon = new System.Windows.Forms.NotifyIcon();

        public MainWindow()
        {
            //Check if another instance is running
            Process[] pname = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
            if (pname.Length > 1)
            {
                MessageBox.Show("Another instance is already running.");
                Application.Current.Shutdown();
            }

            InitializeComponent();

            //Initialize Notify Icon
            tBarIcon.Icon = AMDB_Task_Monitor.Properties.Resources.twrk;
            tBarIcon.DoubleClick += TBarIcon_DoubleClick;
            tBarIcon.BalloonTipClicked += TBarIcon_DoubleClick;
            tBarIcon.Visible = true;

            //Disable Pause Button
            btnPause.IsEnabled = false;

            //Disable future dates
            selectedDate.DisplayDateEnd = DateTime.Now;

            //Set today's date
            selectedDate.SelectedDate = DateTime.Now;

            //Start AutoSave BackGround Worker
            BackGroundUpdate();

            //Start MISC Task
            StartMiscTask();
        }

        private void TBarIcon_DoubleClick(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
            this.Show();
            WindowState = WindowState.Normal;
            this.Activate();
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (tboxTaskName.Text.Trim() != String.Empty)
            {
                try
                {
                    UpdateCurrentTask();

                    AddNewTask();

                    btnPlay.IsEnabled = false;
                    btnPause.IsEnabled = true;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please Fill Current Task", "Empty Task", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UpdateCurrentTask();

                StartMiscTask();

                btnPlay.IsEnabled = true;
                btnPause.IsEnabled = false;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Main_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                this.Hide();
            }
        }

        private void Main_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            tBarIcon.Visible = false;
        }

        private void AddNewTask()
        {
            taskList.Add(new Tasks());
            currentTask = taskList.Count - 1;

            taskListTbl.Items.Add(taskList[currentTask]);
            taskList[currentTask].Name = tboxTaskName.Text.ToUpper();
            taskList[currentTask].StartTime = DateTime.Now;
            taskListTbl.Items.Refresh();

            db.CreateTask(taskList[currentTask].Name,
                taskList[currentTask].StartTime);

            dispTaskList = new List<Tasks>(taskList);

            UpdateSummaryTable();
        }

        private void StartMiscTask()
        {
            taskList.Add(new Tasks());
            currentTask = taskList.Count - 1;

            taskListTbl.Items.Add(taskList[currentTask]);
            taskList[currentTask].Name = "MISC";
            taskList[currentTask].StartTime = DateTime.Now;
            taskListTbl.Items.Refresh();

            db.CreateTask(taskList[currentTask].Name,
                taskList[currentTask].StartTime);

            dispTaskList = new List<Tasks>(taskList);

            UpdateSummaryTable();
        }

        private void UpdateCurrentTask()
        {
            taskList[currentTask].EndTime = DateTime.Now;
            taskList[currentTask].Hours = taskList[currentTask].EndTime - taskList[currentTask].StartTime.TimeOfDay;
            taskListTbl.Items.Refresh();

            db.EndTask(taskList[currentTask].Name,
                    taskList[currentTask].StartTime,
                    taskList[currentTask].EndTime,
                    taskList[currentTask].Hours);

            UpdateSummaryTable();
        }
        
        private void UpdateSummaryTable()
        {
            summaryTbl.Items.Clear();
            
            List<Tasks> summaryList = new List<Tasks>();

            IEnumerable<string> distinctTasks = dispTaskList.Select(x => x.Name).Distinct();

            foreach(string task in distinctTasks)
            {
                Tasks t = new Tasks();
                t.Name = task;
                t.Hours = DateTime.Now.Date;
                summaryList.Add(t);
                summaryTbl.Items.Add(t);
            }
            summaryList.Add(new Tasks());
            summaryTbl.Items.Add(summaryList[summaryList.Count-1]);
            summaryList[summaryList.Count - 1].Name = "Total";
            summaryList[summaryList.Count - 1].Hours = DateTime.Now.Date;

            foreach (Tasks task in dispTaskList)
            {
                foreach (Tasks summary in summaryList)
                {
                    if(task.Name.Trim().Equals(summary.Name.Trim()))
                    {
                        summary.Hours = summary.Hours.Add(task.Hours.TimeOfDay);
                        
                    }
                }
                summaryList[summaryList.Count - 1].Hours = summaryList[summaryList.Count - 1].Hours.Add(task.Hours.TimeOfDay);
            }

            summaryTbl.Items.Refresh();
        }

        #region BackGround Update/Auto Save
        private BackgroundWorker autoUpdater;

        private void BackGroundUpdate()
        {
            autoUpdater = new BackgroundWorker();
            autoUpdater.DoWork += autoUpdater_DoWork;
            autoUpdater.RunWorkerAsync();
            Timer timer = new Timer(60000);
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!autoUpdater.IsBusy)
                autoUpdater.RunWorkerAsync();
        }

        void autoUpdater_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                UpdateCurrentTask();

                Process[] pname = Process.GetProcessesByName("Airport");
                if (pname.Length > 0)
                {
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        if (btnPlay.IsEnabled && !IsActive)
                        {
                            tBarIcon.BalloonTipText = "Airport Tool is Running.\nClick here to open Task Monitor";
                            tBarIcon.ShowBalloonTip(10000);
                        }
                    }));
                }
            }));
        }
        #endregion

        private void selectedDate_Changed(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            taskListTbl.Items.Clear();

            if (DateTime.Parse(selectedDate.SelectedDate.ToString()) != DateTime.Now.Date)
            {
                if (btnPlay.IsEnabled == true || btnPause.IsEnabled == true)
                {
                    prevPlayStatus = btnPlay.IsEnabled;
                    prevPauseStatus = btnPause.IsEnabled;
                }

                btnPlay.IsEnabled = false;
                btnPause.IsEnabled = false;

                dispTaskList.Clear();

                //Load previous entries
                dispTaskList = db.GetTasks(DateTime.Parse(selectedDate.SelectedDate.ToString()));
                foreach (Tasks t in dispTaskList)
                {
                    taskListTbl.Items.Add(t);
                }
                taskListTbl.Items.Refresh();
            }
            else
            {
                btnPlay.IsEnabled = prevPlayStatus;
                btnPause.IsEnabled = prevPauseStatus;

                if (taskList.Count == 0)
                {
                    //Load previous entries
                    taskList = db.GetTasks(DateTime.Parse(selectedDate.SelectedDate.ToString()));
                }

                foreach (Tasks t in taskList)
                {
                    taskListTbl.Items.Add(t);
                }
                currentTask = taskList.Count - 1;
                taskListTbl.Items.Refresh();

                dispTaskList = new List<Tasks>(taskList);
            }

            UpdateSummaryTable();
        }
    }
}
